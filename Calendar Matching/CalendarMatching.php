<?php
include "CalendarMatching.inc"; 

if (!isset($argv[1])) {
	echo "Usage: php CalendarMatching.php <verbosity>\n";
	echo "Script which reads the Input Variables contained within 'CalendarMatching.inc' and outputs the meeting results.\n";
	echo "Please type 'y' as the first argument to see the 'nice print' version.\n";
	echo "All other arguments will return the requested output from the homework... including the odd quotations.\n";
	echo "\n\n If I was supposed to allow the strange quotations in the input... well, I considered it, but eh.\n";
	exit;
}

// tricky!  The endTime is actually an available meeting minute!

// I built these functions so I could test my "math" as I went along
function whatMinute( $timeString) {
	$timeArray = explode(':', $timeString);
	if ($timeArray[0]<0||$timeArray[0]>23)
		exit("Bad Input.  I'm not making this little script do proper error handling...");
	return ($timeArray[0]*60)+$timeArray[1];  
}
function whatTime($theMinuteNumber) {
	$theHour = intdiv($theMinuteNumber,60);
	$theMinute = $theMinuteNumber%60;
	return sprintf("%02d",$theHour).":".sprintf("%02d",$theMinute);
}

// find the merged bounds for the day
$earliestStart = whatMinute($dailyBounds1[0]);
if (whatMinute($dailyBounds2[0]) > $earliestStart)
	$earliestStart=whatMinute($dailyBounds2[0]);

$latestEnd = whatMinute($dailyBounds1[1]);
if (whatMinute($dailyBounds2[1]) < $latestEnd)
	$latestEnd=whatMinute($dailyBounds2[1]);
$dayLength = $latestEnd-$earliestStart;

if ($dayLength<$meetingDuration)
	exit("Nope, no possibility of meeting today");

// a very smart person I used to work with was an expert in reservation availability coding
// and he expressed frustration over the fact that there is really no "clever" way to do it...
// best to just stick with brute force.  So... until someone else I regard as an expert tells 
// me differently... I'm going brute force.  I mean, computers are fast now and we have gobs of 
// memory to use!!!

// start with the whole day available!
$arTheDay= array_fill($earliestStart,$dayLength,1);

// mark false times that aren't available
foreach ($calendar1 as $meeting) {
	for ($i=whatMinute($meeting[0]);$i<(whatMinute($meeting[1]));$i++)
		$arTheDay[$i]=0;
}
// I would've preferred a more advanced data structure for the input...
foreach ($calendar2 as $meeting) {
	for ($i=whatMinute($meeting[0]);$i<(whatMinute($meeting[1]));$i++) 
		$arTheDay[$i]=0;
}

// the available time slices are still marked as 1, so pull those out
$arAvailables=array_keys($arTheDay,1);
$arAvailables[]=2000;					// add an extra that is outside the bounds so our last time period gets checked 

$tryStart=0;
$nextMinute=1;
$lastMinute=0;
// while testing for meeting durations, put the answer(s) in a nice format
foreach ($arAvailables as $oneMinute) {
	if ($oneMinute!=$nextMinute) {
		// if we have a jump, is it long enough??
		$sliceDuration=$lastMinute-$tryStart+1;  // add one because we count the start minute!!
		if ($sliceDuration>=$meetingDuration) {
			$arSlices[] = array( 'StartStart'=>whatTime($tryStart), 
								'StartEnd'=>whatTime($tryStart+$sliceDuration-$meetingDuration),
								'EndStart'=>whatTime($tryStart+$meetingDuration),
								'EndEnd'=>whatTime($tryStart+$sliceDuration)
								);
		}
		$tryStart=$oneMinute;
	}
	$lastMinute=$oneMinute;
	$nextMinute=$oneMinute+1;		// so we know if we've had an increment of 1 in the next array value
}

if ($argv[1]==='y') {
$numMeetings = count($arSlices);
	if ($numMeetings==0)
		exit("Nope, no possibility of meeting today");

	echo "There are ".$numMeetings." meeting opportunities found: \n\n";

	foreach ($arSlices as $num=>$oneSlot) {
		if ($oneSlot['StartStart']==$oneSlot['StartEnd']) {
			echo "\tYou can squeeze in a meeting starting at ".$oneSlot['StartStart']." (ending at ".$oneSlot['EndStart'].")\n\n";
		} else {
			echo "\tThere is a meeting opportunity starting at ".$oneSlot['StartStart'].".
		Start this meeting anytime between ".$oneSlot['StartStart']." and ".$oneSlot['StartEnd'].".
		The meeting will end anytime between ".$oneSlot['EndStart']." and ".$oneSlot['EndEnd']."\n\n";
		}
	}
} else {
	$outputString = '[';
	$firstOne = true;
	foreach ($arSlices as $num=>$oneSlot) {
		if ($firstOne!==true) $outputString .= ", ";
		$firstOne=false;
		$outputString .= "['".$oneSlot['StartStart'].", '".$oneSlot['EndEnd']."']";
	}
	$outputString .= ']';
	
	exit ($outputString);
	
}

?>